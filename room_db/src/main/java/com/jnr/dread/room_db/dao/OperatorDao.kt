package com.jnr.dread.room_db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jnr.dread.commons.dbmodels.DatabaseOperator

@Dao
interface OperatorDao {
    @Query("select * from operator ORDER BY id ASC")
    fun getAllOperators(): LiveData<List<DatabaseOperator>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg operators: DatabaseOperator)
}