package com.jnr.dread.room_db

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jnr.dread.commons.dbmodels.DatabaseOperator
import com.jnr.dread.room_db.dao.OperatorDao

@androidx.room.Database(
    entities = [DatabaseOperator::class],
    version = 14,
    exportSchema = false
)
abstract class Database : RoomDatabase() {

    abstract val operatorDao: OperatorDao

    companion object {

        @Volatile
        private var INSTANCE: Database? = null

        fun getInstance(context: Context): Database {
            synchronized(this) {

                var instance = INSTANCE

                if (instance == null) {

                    instance = Room.databaseBuilder(
                        context,
                        Database::class.java,
                        RCS_DATABASE_NAME
                    )
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance

                }

                return instance
            }
        }

        private const val RCS_DATABASE_NAME = "sample.db"

    }
}